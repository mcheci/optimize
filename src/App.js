import React, { Component } from 'react';
import Modal from 'react-modal';
import { Tooltip, CartesianGrid, LineChart, Line, Legend, XAxis, YAxis } from 'recharts';
import ExperimentForm from './ExperimentForm';
import ResultForm from './ResultForm';
import logo from './brlo-logo.png';
import Annealing from './optimization/Annealing';
import Experiment from './optimization/Experiment';
import './App.css';

export default class App extends Component {
  constructor(props) {
    super(props);

    // Set another state for other optimization problems.
    this.state = this.makeBeerState();
  }

  makeBeerState() {
    const initialExperiments = [
      {
        id: 1,
        inputs: {
          yeastType: 'Yeast α',
          yeastViability: 1234,
          yeastConcentration: 234,
          temperature: 23.3,
          pressure: 12.3,
        },
        results: {
          fermentationTime: 7.5,
          heatingCost: 3892,
        },
        accepted: true,
      },
      {
        id: 2,
        inputs: {
          yeastType: 'Yeast α',
          yeastViability: 1234,
          yeastConcentration: 234,
          temperature: 23.3,
          pressure: 12.3,
        },
        results: {
          fermentationTime: 7.0,
          heatingCost: 3632,
        },
        accepted: true,
      },
      {
        id: 3,
        inputs: {
          yeastType: 'Yeast α',
          yeastViability: 1234,
          yeastConcentration: 234,
          temperature: 23.3,
          pressure: 12.3,
        },
        results: {
          fermentationTime: 6.8,
          heatingCost: 3652,
        },
        accepted: true,
      },
    ];

    const yeastTypes = [
      'Yeast α',
      'Yeast β',
      'Yeast γ',
      'Awesome Yeast',
      'Super Mega Yeast',
    ];

    const parameters = [
      { name: 'Yeast Type', propName: 'yeastType', type: 'enum', options: yeastTypes },
      { name: 'Yeast Viability', propName: 'yeastViability', type: 'number' },
      { name: 'Yeast Concentration', propName: 'yeastConcentration', type: 'number' },
      { name: 'Temperature', propName: 'temperature', type: 'number' },
      { name: 'Pressure', propName: 'pressure', type: 'number' },
    ];

    const results = [
      { name: 'Fermentation Time (Days)', propName: 'fermentationTime', type: 'number' },
      { name: 'Heating Cost (€)', propName: 'heatingCost', type: 'number' },
    ];

    const beers = [
      { name: 'BRLO Pale Ale' },
      { name: 'BRLO Helles' },
      { name: 'BRLO Berliner Weisse' },
      { name: 'BRLO Porter' },
      { name: 'BRLO German IPA' },
      { name: 'BRLO Redlight Ale' },
      { name: 'BRLO Naked' },
    ];

    const lastExperiment = initialExperiments[initialExperiments.length-1];

    const annealing = new Annealing({
      currentState: new Experiment(lastExperiment),
    });

    return {
      initialExperiments,
      parameters,
      results,
      beers,
      annealing,
    };
  }

  render() {
    return (
      <Problem
        initialExperiments={this.state.initialExperiments}
        parameters={this.state.parameters}
        results={this.state.results}
        beers={this.state.beers}
        annealing={this.state.annealing}
      />
    );
  }
}

class Problem extends Component {
  constructor(props) {
    super(props);

    this.state = {
      experiments: props.initialExperiments,
      editingExperiment: null,
      // TODO: This represents a experiment whose result is being edited. Come up with a better name...
      editingResult: null,
    };
  }

  showExperimentModal() {
    this.setState({
      editingExperiment: this.randomExperiment(),
    });
  }

  saveExperiment(experiment) {
    const newExperiment = {
      id: Math.max(...this.state.experiments.map(e => e.id)) + 1,
      inputs: experiment,
    };
    this.setState({
      editingExperiment: null,
      experiments: [...this.state.experiments, newExperiment],
    });
  }

  showResultModal(experiment) {
    this.setState({
      editingResult: experiment,
    });
  }

  saveResult(experiment, result) {
    const experimentWithResults = {
      ...experiment,
      results: result,
    };

    const experiments = this.state.experiments.map(ex => {
      if (ex === experiment) {
        return experimentWithResults;
      } else {
        return ex;
      }
    });
    this.setState({
      editingResult: null,
      experiments,
    });

    const annealing = this.props.annealing;
    const experimentObj = new Experiment(experimentWithResults);

    const accepted = annealing.evaluateNext(experimentObj);
    experimentWithResults.accepted = accepted;
  }

  closeModal() {
    this.setState({
      editingExperiment: null,
      editingResult: null,
    });
  }

  randomExperiment() {
    const annealing = this.props.annealing;
    const nextExperiment = annealing.generateNext();

    return Object.assign({}, nextExperiment.inputs);
  }

  calcScore(experiment) {
    return new Experiment(experiment).cost();
  }

  render() {
    const experiments = this.state.experiments.map((experiment, i) => {
      const inputCells = this.props.parameters.map((param) => (
        <td key={param.propName}>{experiment.inputs[param.propName]}</td>
      ));
      const resultCells = experiment.results ? [...this.props.results.map((result, i) => (
        <td key={result.propName}>{experiment.results[result.propName]}</td>
      )), <td key="score">{this.calcScore(experiment).toFixed(3)}</td>] : [
        <td key="result" colSpan={this.props.results.length}></td>,
        <td key="score">
          <button
            type="button"
            className="btn btn-beer"
            onClick={() => this.showResultModal(experiment)}
          >
            Input Result
          </button>
        </td>
      ];
      return (
        <tr key={i} className={experiment.results && (experiment.accepted ? 'accepted':'rejected')}>
          <th key="id">{experiment.id}</th>
          {inputCells}
          {resultCells}
        </tr>
      );
    });

    return (
      <div className="container">
        <h1>
          <img src={logo} alt="BRLO logo" />
        </h1>
        <div className="row header">
          <div className="col-md-8">
            {this.props.beers && (
              <form className="form-inline">
                <div className="form-group">
                  <label>Beer:</label>
                  <select className="form-control">
                    {this.props.beers.map((beer, i) => (
                      <option key={i}>{beer.name}</option>
                    ))}
                  </select>
                </div>
              </form>
            )}
          </div>
        </div>
        <table className="table">
          <thead>
            <tr>
              <th key="id">#</th>
              {this.props.parameters.map(param => (
                <th key={param.propName}>{param.name}</th>
              ))}
              {this.props.results.map(result => (
                <th key={result.propName} className="text-beer">{result.name}</th>
              ))}
              <th key="score" className="text-beer">Score</th>
            </tr>
          </thead>
          <tbody>
            {experiments}
          </tbody>
        </table>

        {this.state.experiments.every((experiment) => experiment.results) && (
          <div className="row header">
            <div className="col-md-12 text-right">
              <button
                type="button"
                className="btn btn-primary"
                onClick={() => this.showExperimentModal()}
              >
                <span className="glyphicon glyphicon-plus" aria-hidden="true"></span>
              {" New Batch"}
              </button>
            </div>
          </div>
        )}

        {this.renderChart()}
        <Modal
          isOpen={!!this.state.editingExperiment || !!this.state.editingResult}
          contentLabel="Modal"
        >
          {this.state.editingExperiment && (
            <ExperimentForm
              editing={this.state.editingExperiment}
              parameters={this.props.parameters}
              onClose={() => this.closeModal()}
              onSave={ex => this.saveExperiment(ex)}
            />
          )}
          {this.state.editingResult && (
            <ResultForm
              results={this.props.results}
              onClose={() => this.closeModal()}
              onSave={result => this.saveResult(this.state.editingResult, result)}
            />
          )}
        </Modal>
      </div>
    );
  }

  renderChart() {
    const data = this.state.experiments.map(ex => ({
      id: ex.id,
      score: ex && this.calcScore(ex),
    }));
    return (
      <div className="chart-wrapper">
        <LineChart width={800} height={300} data={data}>
          <Line type="monotone" dataKey="score" stroke="#000000" strokeWidth="3" />
          <CartesianGrid stroke="#ccc" strokeDasharray="5 5" />
          <XAxis dataKey="id" />
          <YAxis />
          <Legend verticalAlign="top" height={36} />
          <Tooltip />
        </LineChart>
      </div>
    );
  }
}
