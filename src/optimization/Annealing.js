class Annealing {
  constructor(params = {}) {
    this.stepN = params.stepN || 0;
    this.stepMax = params.stepMax || 100;
    this.currentState = params.currentState;
  }

  generateNext() {
    return this.currentState.mutate();
  }

  evaluateNext(nextState) {
    const currentState = this.currentState;

    const currentCost = currentState.cost();
    const nextCost = nextState.cost();

    const acceptanceProbability = this.acceptanceProbability(currentCost, nextCost);

    const accepted = acceptanceProbability >= Math.random();

    if (accepted) {
      // accept
      this.currentState = nextState;
    }

    this.stepN += 1;

    return accepted;
  }

  acceptanceProbability(currentCost, nextCost) {
    const temperature = this.currentTemperature();

    if (nextCost < currentCost) { return 1; }
    return Math.exp( (currentCost - nextCost) / temperature );
  }

  currentTemperature() {
    return 0.2 * (1-(Math.min(this.stepN, this.stepMax) / this.stepMax));
  }

}

export default Annealing;