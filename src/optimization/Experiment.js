class Experiment {
  constructor(params = {}) {
    params = params || {};

    Object.assign(this, {
      id: params.id,

      inputs: params.inputs || {
        // fake input
        score: 0.5,
      },
      results: params.results || {},
    });
  }

  cost() {
    // TODO: fixme
    return (1 - (this.results.fermentationTime / 10 + this.results.heatingCost / 100000));
  }

  mutate() {
    return new Experiment({
      inputs: {
        yeastType: this.inputs.yeastType,
        yeastViability: varyBy(this.inputs.yeastViability, 0.05),
        yeastConcentration: varyBy(this.inputs.yeastConcentration, 0.02),
        temperature: varyBy(this.inputs.temperature, 0.02),
        pressure: varyBy(this.inputs.pressure, 0.02),
      }
    });
  }
}

function varyBy(value, percentDelta) {
  return Math.round(value * (1 + percentDelta * Math.random() - percentDelta / 2));
}

export default Experiment;