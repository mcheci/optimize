import React, { Component } from 'react';

export default class ExperimentForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      thinking: true,
      editing: props.editing,
    };
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({
        thinking: false,
      });
    }, 3000);
  }

  save() {
    this.props.onSave(this.state.editing);
  }

  updateEditing(param, value) {
    this.setState({
      editing: Object.assign(this.state.editing, {
        [param.propName]: this.convertValue(param, value),
      }),
    });
  }

  convertValue(param, value) {
    switch (param.type) {
      case 'number':
        return parseFloat(value);
      default:
        return value;
    }
  }

  render() {
    if (this.state.thinking) {
      return this.renderThinking();
    }

    const controls = this.props.parameters.map((param, i) => {
      switch (param.type) {
        case 'enum':
          return (
            <div className="form-group" key={param.propName}>
              <label>{param.name}</label>
              <select
                className="form-control"
                value={this.state.editing[param.propName]}
                onChange={e => this.updateEditing(param, e.target.value)}
              >
                {param.options.map((option, i) => <option key={i}>{option}</option>)}
              </select>
            </div>
          );
        case 'number':
          return (
            <div className="form-group" key={param.propName}>
              <label>{param.name}</label>
              <input
                type="number"
                className="form-control"
                value={this.state.editing[param.propName]}
                onChange={e => this.updateEditing(param, e.target.value)}
              />
            </div>
          );
        default:
          return null;
      }
    });

    return (
      <div className="modal-content">
        <div className="modal-header">
          <h4 className="modal-title">New Batch</h4>
        </div>
        <div className="modal-body">
          <form>
            {controls}
          </form>
        </div>
        <div className="modal-footer">
          <button
            type="button"
            className="btn btn-default"
            onClick={this.props.onClose}
          >
            Cancel
          </button>
          <button
            type="button"
            className="btn btn-beer"
            onClick={() => this.save()}
          >
            Save
          </button>
        </div>
      </div>
    );
  }

  renderThinking() {
    return (
      <div className="modal-content">
        <div className="modal-header">
          <h4 className="modal-title">New Batch</h4>
        </div>
        <div className="modal-body">
          <p className="thinking">
            <span className="thinking-emoji">🤔</span>
          </p>
          <p className="thinking">Thinking...</p>
        </div>
      </div>
    );
  }
}
